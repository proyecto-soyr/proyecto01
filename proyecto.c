#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    
    if (argc < 3) { //Cantidad de argumento que se deben recibir
        printf("Se necesitan más argumentos: número de procesos productores, números de procesos consumidores, capacidad del buffer, número de veces que produce el productor y números de veces que consume el consumidor %s\n", argv[0]);
        exit(1);
    }

    int NP = atoi(argv[1]); //Primer argumento en la linea de comando, número de productores
    // Proceso padre
    printf("Se crean %d productores\n", NP);

    for (int i = 0; i < NP; i++) {

        if (fork() == 0) {
            //Procesos hijos
            printf("Proceso productor %d\n", i+1);
            exit(0);
        }
        
    }


    int NC = atoi(argv[2]); //Segundo argumento en la linea de comando, número de consumidores
    // Proceso padre
    printf("Se crean %d consumidores\n", NC);

    for (int i = 0; i < NC; i++) {

        if (fork() == 0) {
            //Procesos hijos
            printf("Proceso consumidor %d\n", i+1);
            exit(0);
        }
        

    }
    

    return 0;
}
